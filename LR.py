import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# Load dữ liệu từ file data.csv với định dạng ký tự là utf-8 và ngăn cách bằng dấu ";"
data = pd.read_csv('./data.csv',encoding='utf-8', sep=';')
# Tạo một mảng one có hình độ dài bằng cột đầu tiên của data với giá trị mặc định là 1
one = np.ones((data.shape[0],1))
# Chèn thêm cột A với value = one
data.insert(loc=0, column='A', value = one)

print(data)

# Tách tập dữ liệu ra thành cặp (data, lable) tương ứng với (data_X, data_y)
# Lấy dữ liệu cột A và Height
data_X = data[["A","Height"]]
# Lấy dữ liệu cộ Weight
data_y = data["Weight"]

# Chia tập dữ liệu ra thành 2 tập train và test. Trong đó tập test được lấy ngẫu nhiên với size là 5
X_train, X_test, y_train, y_test = train_test_split(data_X, data_y, test_size=5)

# Sử dụng thuật toán Linear Regression với tùy chọn không tính sai số (fit_intercept=False)
regr = linear_model.LinearRegression(fit_intercept=False)
# Train thuật toán với tập dữ liệu train
regr.fit(X_train, y_train)
# Đưa ra outcome với tập dữ liệu test
y_pred = regr.predict(X_test)

print(y_pred)

# Tạo biểu đồ các điểm dữ liệu với chấm đỏ và nối các điểm lại
plt.plot(data.Height, data.Weight, 'ro-')
# Vẽ đường thẳng outcome với đường màu xanh
plt.plot(X_test.Height, y_pred, color='blue')

# Hàm hồi quy tuyến tình
w_0 = regr.coef_[0]
w_1 = regr.coef_[1]
# 
x0 = np.linspace(145, 185, 2)
y0 = w_0 + w_1*x0
# Hiển thị đường hồi quy tuyến tính vừa tìm được
plt.plot(x0, y0, color='pink')
# Hiển thị biểu đồ
plt.show()