import numpy as np
import matplotlib.pyplot as plt

# Tạo ma trận X với kích thước nx1
X = np.array(
    [[147, 150, 153, 155, 158, 160, 163, 165, 168, 170, 173, 175, 178, 180, 183]]
).T
# Tạo vector cột y
y = np.array([[49, 50, 51, 53, 54, 56, 58, 59, 60, 71, 63, 64, 66, 67, 70]]).T
# Tạo vector cột với giá trị là 1
one = np.ones((X.shape[0], 1))

# Them bias vào ma trận X
X_ = np.concatenate(
    (one, X), axis=1
)  # Thêm mảng one vừa tạo vào ma trận X theo chiều dọc <axis = 0: ngang, 1: dọc>
A = np.dot(X_.T, X_)  # Ma trận chuyển vị của X_ nhân bới X_
b = np.dot(X_.T, y)  # Ma trận chuyển vị của X_ nhân bới y
w = np.dot(np.linalg.pinv(A), b)  # Ma trận nghịch đảo của A nhân với ma trận b

w_0 = w[0][0]
w_1 = w[1][0]

# Điểm bắt đầu có giá trị là 145, kết thúc là 185, khoảng cách tối đa từ điểm đến đường là 2
x0 = np.linspace(145, 185, 2)
y0 = w_0 + w_1 * x0  # Đường hồi quy tuyến tính

plt.plot(X.T, y.T, "ro")
plt.plot(x0, y0, "b")
plt.axis([140, 190, 45, 75])
plt.xlabel("Height (cm)")
plt.ylabel("Weight (kg)")
plt.show()

y0 = w_0 + w_1 * 159
print("Kết quả:", y0)
