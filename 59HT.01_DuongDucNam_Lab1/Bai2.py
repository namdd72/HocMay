import numpy as np
import matplotlib.pyplot as plt

# Tạo ma trận với kích thước nx1
X = np.array([[17.5, 15.6, 9.8, 5.3, 7.9, 10, 19.2, 13.1]]).T
y = np.array([[14.2, 11.7, 6.4, 2.1, 4.8, 8.1, 15.4, 9.8]]).T
# Tạo vector cột có giá trị 1
one = np.ones((y.shape[0], 1))
# Thêmm bias vào ma trận X
y_ = np.concatenate(
    (one, y), axis=1
)  # Thêm mảng one vừa tạo vào ma trận X theo chiều dọc <axis = 0: ngang, 1: dọc>
A = np.dot(y_.T, y_)  # Ma trận chuyển vị của y_ nhân với y_
b = np.dot(y_.T, X)  # Ma trận chuyển vị của y_ nhân với X
w = np.dot(np.linalg.pinv(A), b)  # Ma trận nghịch đảo của A nhân với ma trận b

w_0 = w[0][0]
w_1 = w[1][0]

# Điểm bắt đầu có giá trị là 1, kết thúc là 16, khoảng cách tối đa từ điểm đến đường là 2
y0 = np.linspace(1, 16, 2)
x0 = w_0 + w_1 * y0  # Đường hồi quy tuyến tính
plt.plot(y.T, X.T, "ro")
plt.plot(y0, x0, "b")
plt.axis([0, 17, 3, 20])
plt.xlabel("y")
plt.ylabel("X")
plt.show()

y0 = (11 - w_0) / w_1
print("Kết quả:", y0)
