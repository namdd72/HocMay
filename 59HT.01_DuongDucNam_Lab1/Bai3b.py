import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import linear_model, datasets
from sklearn.model_selection import train_test_split

# Load dữ liệu từ file winequality-red.csv với định dạng ký tự utf8 các dữ liệu được ngăn cách bởi dấu ;
data = pd.read_csv("winequality-red.csv", encoding="utf-8", sep=";")

# Sử dụng thuật toán hồi quy tuyến tính để dự đoán từ dữ liệu đưa vào
regr = linear_model.LinearRegression()

# Tạo dataframe chỉ chứa data làm biến giải thích
wine_except_quality = data.drop("quality", axis=1)
X = wine_except_quality

# Sử dụng quality làm biến mục tiêu
Y = data["quality"]

# Tạo model
regr.fit(X, Y)

# Hiển thị hệ số hồi quy (sắp xếp theo thứ tự tăng dần của hệ số hồi quy)
print(
    pd.DataFrame(
        {"Tên": wine_except_quality.columns, "Hệ số hồi quy": regr.coef_}
    ).sort_values(by="Hệ số hồi quy")
)

# Sai số
print("Sai số:", regr.intercept_)

print("Phương trình hồi quy:")
print(
    "y = {} + {}".format(
        " + ".join(
            [
                "{}*'{}'".format(regr.coef_[i], wine_except_quality.columns[i])
                for i in range(len(regr.coef_))
            ]
        ),
        regr.intercept_,
    )
)
