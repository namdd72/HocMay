import numpy as np
import matplotlib.pyplot as plt
from sklearn import neighbors, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# Lấy dữ liệu có sẵn từ thư viện sklearn
iris = datasets.load_iris()
iris_X = iris.data
iris_y = iris.target

print("Số lớp:", len(np.unique(iris_y)))
print("Số đối tượng của tập dữ liệu:", len(iris_y))

X0 = iris_X[iris_y == 0, :]
print("Mẫu từ lớp 0 :\n", X0[:5, :])

X1 = iris_X[iris_y == 1, :]
print("Mẫu từ lớp 1:\n", X1[:5, :])

X2 = iris_X[iris_y == 2, :]
print("Mẫu từ lớp 2:\n", X2[:5, :])

# Lấy ngẫu nhiên 50 điểm dữ liệu ra để test
X_train, X_test, y_train, y_test = train_test_split(iris_X, iris_y, test_size=50)

# Sử dụng thuật toán KNN để với điểm lân cận là 1
clf = neighbors.KNeighborsClassifier(n_neighbors=1, p=2)
# Tranning thuật toán với tập dữ liệu train có nhãn
clf.fit(X_train, y_train)
# Đưa ra dự đoán từ dữ liệu đưa vào
y_pred = clf.predict(X_test)

# In ra label dự đoán và label thật của test data
print("Hiển thị kết quả của 20 điểm dữ liệu test:")
print("Nhãn dự đoán: ", y_pred[20:40])
print("Thực tế: ", y_test[20:40])

# So sánh kết quả dự đoán với kết quả chính xác xem tỷ lệ dự đoán đúng là bao nhiêu
print(
    "Độ chính xác của 1 điểm lân cận: %.2f %%" % (100 * accuracy_score(y_test, y_pred))
)

# Tăng số lượng điểm lân cận lên 10 để tăng độ chính xác do các điểm lân cận có thể là điểm nhiễu
clf = neighbors.KNeighborsClassifier(n_neighbors=10, p=2)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

print(
    "Độ chính xác của 10 điểm lân cận: %.2f %%" % (100 * accuracy_score(y_test, y_pred))
)

# Đánh trọng số cho các điểm lân cận dựa trên khoảng cách của điểm cần dự đoán và điểm lân cận
clf = neighbors.KNeighborsClassifier(n_neighbors=10, p=2, weights="distance")
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

# Tỷ lệ chính xác của thuật toán với phương pháp đánh trọng số
print(
    "Độ chính xác của 10 điểm lân cận (1/distance weights): %.2f %%"
    % (100 * accuracy_score(y_test, y_pred))
)

# Định nghĩa một hàm đánh trọng số cho các điểm
def myweight(distances):
    sigma2 = 0.5
    return np.exp(-(distances ** 2) / sigma2)


# Sử dụng hàm vừa định nghĩa để đánh trọng số cho các điểm lân cận
clf = neighbors.KNeighborsClassifier(n_neighbors=10, p=2, weights=myweight)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

# Tỷ lệ chính xác của thuật toán với phương pháp đánh trọng số bằng hàm tự định nghĩa
print(
    "Độ chính xác của 10 điểm lân cận (customized weights): %.2f %%"
    % (100 * accuracy_score(y_test, y_pred))
)

