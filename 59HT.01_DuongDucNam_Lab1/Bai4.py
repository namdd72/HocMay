import pandas as pd
import sklearn
from sklearn.linear_model import LinearRegression
from sklearn.datasets import load_boston

# import dữ liệu boston housing từ thư viện sklearn
boston = load_boston()
# Chuyển dữ liệu về dạng pandas
bos = pd.DataFrame(boston.data)
# Thêm cột 'PRICE' vào bảng số liệu
bos.columns = boston.feature_names
bos["PRICE"] = boston.target

X = bos.drop("PRICE", axis=1)  # Giá trị dự báo X
Y = bos["PRICE"]  # Giá trị đích Y

# Hàm hồi quy tuyến tính
lm = LinearRegression()
lm.fit(X, Y)

# Hệ số w của các thuộc tính
print(pd.DataFrame({"Tên": X.columns, "Hệ số W": lm.coef_}).sort_values(by="Hệ số W"))
print("w0 =", lm.intercept_)

print("Phương trình hồi quy tuyến tính:")
print(
    "y = {} + {}".format(
        " + ".join(
            ["{}*{}".format(lm.coef_[i], X.columns[i]) for i in range(len(lm.coef_))]
        ),
        lm.intercept_,
    )
)
