import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import linear_model, datasets
from sklearn.model_selection import train_test_split

# Load dữ liệu từ file winequality-red.csv với định dạng ký tự utf8 các dữ liệu được ngăn cách bởi dấu ;
data = pd.read_csv("winequality-red.csv", encoding="utf-8", sep=";")
# Khởi tạo mảng mới có giá trị 1 và số lượng, hình dáng của mảng tương ứng với một cột trong dữ liệu đầu vào
one = np.ones((data.shape[0], 1))
# Chèn thêm cột tên 'bias' và vị trí 0 có giá trị bằng mảng one
data.insert(loc=0, column="bias", value=one)
X = data[["bias", "density"]]
y = data["alcohol"]

# Tách training và test set, kích thước của test là 33.33%
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33)
# Sử dụng thuật toán hồi quy tuyến tính để dự đoán từ dữ liệu đưa vào
regr = linear_model.LinearRegression(
    fit_intercept=False
)  # Tính toán sai số => False (mặc định = True)
regr.fit(X_train, y_train)
# Đưa ra dự đoán
Y_pred = regr.predict(X_test)

# Biểu đồ phân tán
plt.scatter(data.density, data.alcohol, c="b")

# Hiển thị đường dự đoán đen theo các mẫu thử ngẫu nhiên được đưa vào test
plt.plot(X_test.density, Y_pred, c="black")

# Hàm hồi quy tuyến tính
w_0 = regr.coef_[0]
w_1 = regr.coef_[1]
# Điểm bắt đầu có giá trị là 0.99, kết thúc là 0.996, ép kiểu về float 64
x0 = np.linspace(0.99, 0.996, dtype="float64")
y0 = w_0 + w_1 * x0

# Hiển thị đường hồi quy tuyến tính màu hồng vừa tìm được
plt.plot(x0, y0, color="pink")
# Hiển thị toàn bộ biểu đồ kết quả
plt.show()
