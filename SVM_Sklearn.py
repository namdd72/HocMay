from sklearn.svm import SVC
import numpy as np

np.random.seed(22)

means = [[2, 2], [4, 2]]
cov = [[0.3, 0.2], [0.2, 0.3]]
N = 10

X0 = np.random.multivariate_normal(means[0], cov, N)
X1 = np.random.multivariate_normal(means[1], cov, N)
X = np.concatenate((X0.T, X1.T), axis=1)
y = np.concatenate((np.ones((1, N)), -1 * np.ones((1, N))), axis=1)

y1 = y.reshape((2 * N,))
X1 = X.T
clf = SVC(kernel="linear", C=1e5)

clf.fit(X1, y1)

w = clf.coef_
b = clf.intercept_
print("w = ", w)
print("b = ", b)
