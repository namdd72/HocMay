import numpy as np
import matplotlib.pyplot as plt
from sklearn import neighbors, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# Lấy dữ liệu có sẵn từ thư viện sklearn
iris = datasets.load_iris()
iris_X = iris.data
iris_Y = iris.target

print('Số lớp:', len(np.unique(iris_Y)))
print('Số đối tượng của tập dữ liệu:', len(iris_Y))

# Lấy ngẫu nhiên 50 điểm dữ liệu ra để test
X_train, X_test, y_train, y_test = train_test_split(iris_X, iris_Y, test_size=50)

# Sử dụng thuật toán KNN để với điểm lân cận là 1
clf = neighbors.KNeighborsClassifier(n_neighbors = 1, p = 2)
# Tranning thuật toán với tập dữ liệu train có nhãn
clf.fit(X_train, y_train)
# Đưa ra dự đoán từ dữ liệu đưa vào
y_pred = clf.predict(X_test)

# Hiển thị lable kết quả và lable dự đoán
print ('Print results for 20 test data points:')
print ('Predicted labels: ', y_pred[20:40])
print ('Ground truth    : ', y_test[20:40])

# So sánh kết quả dự đoán với kết quả chính xác xem tỷ lệ dự đoán đúng là bao nhiêu
print ('Accuracy of 1NN: %.2f %%' %(100*accuracy_score(y_test, y_pred)))

# Tăng số lượng điểm lân cận lên 10 để tăng độ chính xác do các điểm lân cận có thể là điểm nhiễu
clf = neighbors.KNeighborsClassifier(n_neighbors = 10, p = 2)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

print ('Accuracy of 10NN with major voting: %.2f %%' %(100*accuracy_score(y_test, y_pred)))

# Đánh trọng số cho các điểm lân cận dựa trên khoảng cách của điểm cần dự đoán và điểm lân cận
clf = neighbors.KNeighborsClassifier(n_neighbors = 10, p = 2, weights = 'distance')
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

# Tỷ lệ chính xác của thuật toán với phương pháp đánh trọng số
print ('Accuracy of 10NN (1/distance weights): %.2f %%' %(100*accuracy_score(y_test, y_pred)))

# Định nghĩa một hàm đánh trọng số cho các điểm
def myweight(distances):
    sigma2 = .5
    return np.exp(-distances**2/sigma2)

# Sử dụng hàm vừa định nghĩa để đánh trọng số cho các điểm lân cận
clf = neighbors.KNeighborsClassifier(n_neighbors = 10, p = 2, weights = myweight)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

# Tỷ lệ chính xác của thuật toán với phương pháp đánh trọng số bằng hàm tự định nghĩa
print ('Accuracy of 10NN (customized weights): %.2f %%' %(100*accuracy_score(y_test, y_pred)))